import os
import pickle
import numpy as np
from django.conf import settings
from rest_framework import views
from rest_framework import status
from rest_framework.response import Response
from surprise import dump
import pandas as pd
import json


class Predict(views.APIView):

    def calculate_predictions(self,ui, ratings_f, Mapping_file, algorithm):
        from surprise import SVD
        if ui in ratings_f.userId.unique():
            ui_list = ratings_f[ratings_f.userId == ui].movieId.tolist()
            d = {k: v for k, v in Mapping_file.items() if not v in ui_list}
            predictedL = []
            for i, j in d.items():
                predicted = algorithm.predict(ui, j)
                predictedL.append((i, predicted[3]))
            pdf = pd.DataFrame(predictedL, columns=['movies', 'ratings'])
            pdf.sort_values('ratings', ascending=False, inplace=True)
            print(predictedL)
            return pdf.head(5)

        else:
            print("User Id does not exist in the list!") #for debuging use only
            return None

        return algorithm, predictions

    def post(self, request):
        # Load movies data
        movies = pd.read_csv('../dataset/movies.csv')
        tags = pd.read_csv('../dataset/tags.csv')
        ratings = pd.read_csv('../dataset/ratings.csv')

        ratings_f = ratings.groupby('userId').filter(lambda x: len(x) >= 55)
        ratings_f.drop(['timestamp'], 1, inplace=True)

        # Use ratings data to downsample tags data to only movies with ratings
        movies.tail()
        movies['genres'] = movies['genres'].str.replace('|', ' ')

        # list the movie titles that survive the filtering
        movie_list_rating = ratings_f.movieId.unique().tolist()
        movies = movies[movies.movieId.isin(movie_list_rating)]

        Mapping_file = dict(zip(movies.title.tolist(), movies.movieId.tolist()))

        #load the model
        _, loaded_algo = dump.load('../recommender model/model')

        user_id = request.data

        result = self.calculate_predictions(user_id, ratings_f, Mapping_file, loaded_algo)

        fixresult = result.to_json()
        print(result)

        return Response(result, status=status.HTTP_200_OK)
