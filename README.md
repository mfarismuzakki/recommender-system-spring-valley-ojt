## Introduction
This project intented as a practice to implement recommender system on MovieLens Dataset using python through Django REST Framework API.
For now, The project use CodeIgniter to consume the API. 

- Algorithm file codes used to create the model for Recommender System are placed inside "recommender model" folder.
- Movielens Dataset is stored inside "dataset" folder.Default dataset that being used is ml-latest-small
- Django REST Framewok API codes are stored inside "apitest" folder
- Codeigniter Client file codes are stored inside "codeigniter" folder

## Code's workflow

The project use movielens dataset as both training and test dataset. 
the dataset then preprocessed and then trained using SVD algorithm from the Scikit-Surprise Library
The model then saved as file named model located in the same folder where create_the_model.py is located

After the model is created, it can be served through API. For example, a Django REST Framework API is created

The project also has a CodeIgniter Client to consume the API. it works by accessing the endlink where the data needed is being served


## Requirement 

### For Django API
- Python 3.6 +
- Django Framwork 2.6
- rest framework django (newest)

### For creating the prediction model (Using SVD Algorithm)
- Python 3.6 +
- Scikit-Surprise (Python lib)
- Pandas (Python lib)
- Scikit-learn (Python lib)
- numpy (Python lib)

### Client for testing the System 
- Codeigniter 3.1.9 (PHP web framework)
- CURL for Codeignter


## How to use 

First Clone or download the project.

### Creating The prediction model
- Run file create_the_model.py through terminal (cmd, powershell, git bash). By default the creator ml-latest-small dataset inside the dataset folder. 
- Model will be saved at the same folder (named model)
- You can see the accuracy of the model at the end of the prediction process in terminal

### The API webservice to serve the prediction model
1. open the terminal