<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	var $api = 'http://127.0.0.1:8000/api/predict/';


	function __construct(){
		parent::__construct();
		$this->load->library('curl');
	}


	public function index()
	{
		$this->load->view('welcome_message');
	}


	public function post(){

		$data = $this->input->post('number');

		$ch = curl_init($this->api);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_FAILONERROR, true);                                                                    
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data))                                                                       
		);         

        $output=curl_exec($ch);

        curl_close($ch);
		$i=0;
        $tmp = json_decode($output);
		$tmp = get_object_vars($tmp);

		// print_r($output);
		$iyah['data'] = $tmp;
   		$this->load->view('table',$iyah);

	}



}