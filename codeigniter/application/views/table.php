<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<title>null</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="breadcrumb">
				<h2>Top 5 Movies Recommendation for You</h2>
			</div>
			<table class="table table-striped">
				<thead>
					<td>No</td>
					<td>Name</td>
					<td>Rating Prediction</td>
				</thead>
				<tbody>
					<?php $i=1; foreach($data['movies'] as $var){?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $var; ?></td>
						<td><?php echo $data['ratings'][$i-1]; ?></td>
					</tr>
					<?php $i++; } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>



</body>
</html>