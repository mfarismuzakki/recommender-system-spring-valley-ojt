# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 07:51:43 2019

@author: mfarismuzakki
"""
import numpy as np
from itertools import chain
import pandas as pd

#' Calculates rating predictions according to CF formula.
def calculate_predictions(ui,ratings_f,Mapping_file,algorithm):
    from surprise import SVD
    if ui in ratings_f.userId.unique():
        ui_list = ratings_f[ratings_f.userId == ui].movieId.tolist()
        d = {k: v for k, v in Mapping_file.items() if not v in ui_list}
        predictedL = []
        for i, j in d.items():
            predicted = algorithm.predict(ui, j)
            predictedL.append((i, predicted[3]))
        pdf = pd.DataFrame(predictedL, columns=['movies', 'ratings'])
        pdf.sort_values('ratings', ascending=False, inplace=True)
        pdf.set_index('movies', inplace=True)
        return pdf.head(5)
    else:
        print("User Id does not exist in the list!")
        return None




def find_similarities(latent_matrix_1_df, latent_matrix_2_df, similarity_metric, make_positive_similarities, k):
    from sklearn.metrics.pairwise import cosine_similarity
    # take the latent vectors for a selected movie from both content
    # and collaborative matrixes
    a_1 = np.array(latent_matrix_1_df.loc['Strada, La (1954)']).reshape(1, -1)
    a_2 = np.array(latent_matrix_2_df.loc['Strada, La (1954)']).reshape(1, -1)

    # calculate the similartity of this movie with the others in the list
    score_1 = cosine_similarity(latent_matrix_1_df, a_1).reshape(-1)
    score_2 = cosine_similarity(latent_matrix_2_df, a_2).reshape(-1)

    # an average measure of both content and collaborative
    hybrid = ((score_1 + score_2) / 2.0)

    # form a data frame of similar movies
    dictDf = {'content': score_1, 'collaborative': score_2, 'hybrid': hybrid}
    similar = pd.DataFrame(dictDf, index=latent_matrix_1_df.index)
    # sort it on the basis of either: content, collaborative or hybrid,
    # here : content
    similar.sort_values('content', ascending=False, inplace=True)


def add_predictions_to_prediction_matrix(Final):
    from sklearn.feature_extraction.text import TfidfVectorizer
    tfidf = TfidfVectorizer(stop_words='english')
    tfidf_matrix = tfidf.fit_transform(Final['metadata'])
    tfidf_df = pd.DataFrame(tfidf_matrix.toarray(), index=Final.index.tolist())

    # Compress with SVD
    from sklearn.decomposition import TruncatedSVD
    svd = TruncatedSVD(n_components=200)
    latent_matrix = svd.fit_transform(tfidf_df)
    return latent_matrix


def predict_cf(ratings_f):
    from surprise import Dataset, Reader, SVD, dump
    from surprise.model_selection import train_test_split
    import os

    # instantiate a reader and read in our rating data
    reader = Reader(rating_scale=(1, 5))
    data = Dataset.load_from_df(ratings_f[['userId', 'movieId', 'rating']], reader)

    # train SVD on 75% of known rates
    trainset, testset = train_test_split(data, test_size=.25)
    algorithm = SVD()
    algorithm.fit(trainset)
    predictions = algorithm.test(testset)

    file_name = os.path.expanduser('model')
    dump.dump(file_name, algo=algorithm)


    return algorithm,predictions
