from surprise import dump
import pandas as pd
import cf_algorithm

#Load movies data
movies = pd.read_csv('../dataset/movies.csv')
tags = pd.read_csv('../dataset/tags.csv')
ratings = pd.read_csv('../dataset/ratings.csv')


ratings_f = ratings.groupby('userId').filter(lambda x: len(x) >= 55)
ratings_f.drop(['timestamp'],1, inplace=True)

#Use ratings data to downsample tags data to only movies with ratings
movies.tail()
movies['genres'] = movies['genres'].str.replace('|',' ')

# list the movie titles that survive the filtering
movie_list_rating = ratings_f.movieId.unique().tolist()
movies = movies[movies.movieId.isin(movie_list_rating)]


Mapping_file = dict(zip(movies.title.tolist(), movies.movieId.tolist()))

_, loaded_algo = dump.load('model')

user_id = 1
print(cf_algorithm.calculate_predictions(user_id,ratings_f,Mapping_file,loaded_algo))
