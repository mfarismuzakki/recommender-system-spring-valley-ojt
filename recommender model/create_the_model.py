# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 07:51:25 2019

@author: mfarismuzakki
"""
import cf_algorithm
import pandas as pd


def rmse_function(result):
    from surprise import accuracy
    return accuracy.rmse(result)

#Load movies data
movies = pd.read_csv('../dataset/movies.csv')
tags = pd.read_csv('../dataset/tags.csv')
ratings = pd.read_csv('../dataset/ratings.csv')

#Use ratings data to downsample tags data to only movies with ratings
movies.tail()
movies['genres'] = movies['genres'].str.replace('|',' ')

#limit ratings to user ratings that have rated more that 55 movies --
#Otherwise it becomes impossible to pivot the rating dataframe later for collaborative filtering.
ratings_f = ratings.groupby('userId').filter(lambda x: len(x) >= 55)

# list the movie titles that survive the filtering
movie_list_rating = ratings_f.movieId.unique().tolist()

#filter the movies data frame
movies = movies[movies.movieId.isin(movie_list_rating)]

# map movie to id:
Mapping_file = dict(zip(movies.title.tolist(), movies.movieId.tolist()))

tags.drop(['timestamp'],1, inplace=True)
ratings_f.drop(['timestamp'],1, inplace=True)

# create a mixed dataframe of movies title, genres
# and all user tags given to each movie
mixed = pd.merge(movies, tags, on='movieId', how='left')

# create metadata from tags and genres
mixed.fillna("", inplace=True)
mixed = pd.DataFrame(mixed.groupby('movieId')['tag'].apply(lambda x: "%s" % ' '.join(x)))
Final = pd.merge(movies, mixed, on='movieId', how='left')
Final ['metadata'] = Final[['tag', 'genres']].apply(lambda x: ' '.join(x), axis = 1)

#number of latent dimensions to keep
latent_matrix = cf_algorithm.add_predictions_to_prediction_matrix(Final)
n = 200
latent_matrix_1_df = pd.DataFrame(latent_matrix[:,0:n], index=Final.title.tolist())


training,result =  cf_algorithm.predict_cf(ratings_f)


user_id = 80
print("Recomendation for user_id {}".format(user_id))
print(cf_algorithm.calculate_predictions(user_id,ratings_f,Mapping_file,training))
print("Accuration (RMSE): {}".format(rmse_function(result)))
